package com.tenantmanagement.tenantbackend.controller;

import com.tenantmanagement.tenantbackend.exception.ResourceNotFoundException;
import com.tenantmanagement.tenantbackend.model.Tenant;
import com.tenantmanagement.tenantbackend.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class TenantController {

    @Autowired
    private TenantRepository tenantRepository;

    // GET : all tenants
    @GetMapping("/tenants")
    public List<Tenant> getAllTenants() {
        return tenantRepository.findAll();
    }

    // GET : device by id
    @GetMapping("/tenants/{id}")
    public ResponseEntity<Tenant> getTenantById(@PathVariable(value = "id") long tenantId) throws ResourceNotFoundException {
        Tenant tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new ResourceNotFoundException("Tenant Not Found for the Id: " + tenantId));
        return ResponseEntity.ok().body(tenant);
    }

    // POST : new tenant
    @PostMapping("/tenants")
    public Tenant createTenant(@RequestBody Tenant tenant) {
        return tenantRepository.save(tenant);
    }

    // DELETE : delete tenant
    @DeleteMapping("/tenants/{id}")
    public Map<String, Boolean> deleteTenant(@PathVariable(value = "id") long tenantId) throws ResourceNotFoundException {
        Tenant tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new ResourceNotFoundException("Tenant Not Found for the Id:" + tenantId));

        tenantRepository.delete(tenant);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    // PUT : update tenant
    @PutMapping("/tenants/{id}")
    public ResponseEntity<Tenant> updateTenant(@PathVariable(value = "id") Long tenantId,@Validated @RequestBody Tenant tenantDetails) throws ResourceNotFoundException {
        Tenant tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new ResourceNotFoundException("Tenant Not Found for the Id:" + tenantId));

        tenant.setTenantName(tenantDetails.getTenantName());
        tenant.setTenantAddress(tenantDetails.getTenantAddress());
        tenant.setTenantDomain(tenantDetails.getTenantDomain());
        tenant.setTenantUsername(tenantDetails.getTenantUsername());
        tenant.setTenantStatus(tenantDetails.getTenantStatus());
        tenant.setTenantDevices(tenantDetails.getTenantDevices());

        final Tenant updatedTenant = tenantRepository.save(tenant);
        return ResponseEntity.ok(updatedTenant);
    }

}
