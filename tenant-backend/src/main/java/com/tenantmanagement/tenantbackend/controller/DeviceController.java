package com.tenantmanagement.tenantbackend.controller;

import com.tenantmanagement.tenantbackend.exception.ResourceNotFoundException;
import com.tenantmanagement.tenantbackend.model.Device;
import com.tenantmanagement.tenantbackend.model.Tenant;
import com.tenantmanagement.tenantbackend.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1")
public class DeviceController {

    @Autowired
    private DeviceRepository deviceRepository;

    // GET : all the device details
    @GetMapping("/devices")
    public List<Device> getAllDevices() {
        return deviceRepository.findAll();
    }

    // GET : device by id
    @GetMapping("/devices/{id}")
    public ResponseEntity<Device> getDeviceById(@PathVariable(value = "id") long deviceId) throws ResourceNotFoundException {
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException("Device Not Found for the Id: " + deviceId));
        return ResponseEntity.ok().body(device);
    }

    // POST : new device
    @PostMapping("/devices")
    public Device createDevice(@RequestBody Device device) {
        return deviceRepository.save(device);
    }

    // DELETE : delete device
    @DeleteMapping("/devices/{id}")
    public Map<String, Boolean> deleteDevice(@PathVariable(value = "id") long deviceId) throws ResourceNotFoundException {
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException("Device Not Found for the Id:" + deviceId));

        deviceRepository.delete(device);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return response;
    }

    // PUT : update tenant
    @PutMapping("/devices/{id}")
    public ResponseEntity<Device> updateDevice(@PathVariable(value = "id") Long deviceId, @RequestBody Device deviceDetails) throws ResourceNotFoundException {
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException("Tenant Not Found for the Id:" + deviceId));

        device.setDeviceName(deviceDetails.getDeviceName());
        device.setDeviceDescription(deviceDetails.getDeviceDescription());
        device.setDeviceSerialNumber(deviceDetails.getDeviceSerialNumber());
        device.setDeviceStatus(deviceDetails.getDeviceStatus());

        final Device updatedDevice = deviceRepository.save(device);
        return ResponseEntity.ok(updatedDevice);
    }

}
