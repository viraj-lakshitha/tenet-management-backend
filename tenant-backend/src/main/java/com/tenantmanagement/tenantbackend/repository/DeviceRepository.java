package com.tenantmanagement.tenantbackend.repository;

import com.tenantmanagement.tenantbackend.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
}
