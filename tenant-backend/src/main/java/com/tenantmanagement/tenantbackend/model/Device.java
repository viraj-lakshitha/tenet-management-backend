package com.tenantmanagement.tenantbackend.model;

import javax.persistence.*;

@Entity
@Table(name = "device_details")
public class Device {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "device_name", nullable = false)
    private String deviceName;

    @Column(name = "device_description", nullable = false)
    private String deviceDescription;

    @Column(name = "device_serial_number", nullable = false)
    private String deviceSerialNumber;

    @Column(name = "device_status", nullable = false)
    private Boolean deviceStatus;

    public Device() { }

    public Device(String deviceName, String deviceDescription, String deviceSerialNumber, Boolean deviceStatus) {
        this.deviceName = deviceName;
        this.deviceDescription = deviceDescription;
        this.deviceSerialNumber = deviceSerialNumber;
        this.deviceStatus = deviceStatus;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }


    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }


    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }


    public Boolean getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(Boolean deviceStatus) {
        this.deviceStatus = deviceStatus;
    }
}
