package com.tenantmanagement.tenantbackend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tenant_details")
public class Tenant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tenant_name", nullable = false)
    private String tenantName;

    @Column(name = "tenant_description", nullable = false)
    private String tenantDescription;

    @Column(name = "tenant_domain", nullable = false)
    private String tenantDomain;

    @Column(name = "tenant_address", nullable = false)
    private String tenantAddress;

    @Column(name = "tenant_username", nullable = false)
    private String tenantUsername;

    @Column(name = "tenant_status", nullable = false)
    private Boolean tenantStatus;

    @OneToMany(targetEntity = Device.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "device_tenant_id", referencedColumnName = "id")
    List<Device> tenantDevices = new ArrayList<>();

    public Tenant() { }

    public Tenant(String tenantName, String tenantDescription, String tenantDomain, String tenantAddress, String tenantUsername, Boolean tenantStatus) {
        this.tenantName = tenantName;
        this.tenantDescription = tenantDescription;
        this.tenantDomain = tenantDomain;
        this.tenantAddress = tenantAddress;
        this.tenantUsername = tenantUsername;
        this.tenantStatus = tenantStatus;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }


    public String getTenantDescription() {
        return tenantDescription;
    }

    public void setTenantDescription(String tenantDescription) {
        this.tenantDescription = tenantDescription;
    }


    public String getTenantDomain() {
        return tenantDomain;
    }

    public void setTenantDomain(String tenantDomain) {
        this.tenantDomain = tenantDomain;
    }


    public String getTenantAddress() {
        return tenantAddress;
    }

    public void setTenantAddress(String tenantAddress) {
        this.tenantAddress = tenantAddress;
    }


    public String getTenantUsername() {
        return tenantUsername;
    }

    public void setTenantUsername(String tenantUsername) {
        this.tenantUsername = tenantUsername;
    }


    public Boolean getTenantStatus() {
        return tenantStatus;
    }

    public void setTenantStatus(Boolean tenantStatus) {
        this.tenantStatus = tenantStatus;
    }

    public List<Device> getTenantDevices() {
        return tenantDevices;
    }

    public void setTenantDevices(List<Device> tenantDevices) {
        this.tenantDevices = tenantDevices;
    }
}
