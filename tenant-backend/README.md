# Device : REST APIs

1. GET : http://localhost:8080/api/v1/devices :  Retrieve all devices
2. POST : http://localhost:8080/api/v1/devices : Add new device
```
{
    "deviceName": "Samsung S10",
    "deviceDescription" : "Lastest High End Phone",
    "deviceSerialNumber" : "2034893849283",
    "deviceStatus": true  
}
```

3. GET : http://localhost:8080/api/v1/devices/{id} : Retrieve single device details
4. DELETE : http://localhost:8080/api/v1/devices/{id} : Delete single device

----

# Tenant : REST APIs

1. GET : http://localhost:8080/api/v1/tenants :  Retrieve all tenants
2. POST : http://localhost:8080/api/v1/tenants : Add new tenant
```
{
    "tenantName": "John Alex",
    "tenantDescription" : "Undergraduate",
    "tenantUsername" : "alex_11",
    "tenantDomain" : "IT",
    "tenantAddress" : "Sri Lanka",
    "tenantStatus": true  
}
```

3. GET : http://localhost:8080/api/v1/tenants/{id} : Retrieve single tenant details
4. DELETE : http://localhost:8080/api/v1/tenants/{id} : Delete single tenant